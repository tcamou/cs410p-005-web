use crate::questions_database::questions_module::QuestionStructure;
use axum::{
    extract::{Json, Path, State},
    http::StatusCode,
    response::IntoResponse,
};
use serde_json::{json, Value};
use sqlx::PgPool;
use std::sync::Arc;

// fetches all questions from db
pub async fn fetch_all_questions(State(database_pool): State<Arc<PgPool>>) -> impl IntoResponse {
    let all_questions = sqlx::query_as::<_, QuestionStructure>(
        "SELECT * FROM questions_table ORDER BY question_id",
    )
    .fetch_all(&*database_pool)
    .await
    .expect("Error retrieving questions from the database");
    Json(all_questions)
}

// fetch one question by question_id
pub async fn get_question_by_id(
    Path(q_id): Path<i32>,
    State(database_pool): State<Arc<PgPool>>,
) -> impl IntoResponse {
    let query_result = sqlx::query_as::<_, QuestionStructure>(
        "SELECT * FROM questions_table WHERE question_id = $1",
    )
    .bind(q_id)
    .fetch_optional(&*database_pool)
    .await;

    if let Ok(Some(question)) = query_result {
        // if id found, reply with the question
        (StatusCode::OK, Json(question)).into_response()
    } else if let Ok(None) = query_result {
        // if id NOT found, respond with an error
        let error_message =
            json!({"error":"Question with this specific if not found or it doesn't exists!"});
        (StatusCode::NOT_FOUND, Json(error_message)).into_response()
    } else {
        // handle internal server error
        let error_message = json!({"error": "Internal server error"});
        (StatusCode::INTERNAL_SERVER_ERROR, Json(error_message)).into_response()
    }
}

// remove question from db by question_id
pub async fn delete_question(
    Path(q_id): Path<i32>,
    State(database_pool): State<Arc<PgPool>>,
) -> impl IntoResponse {
    let delete_result = sqlx::query("DELETE FROM questions_table WHERE question_id = $1")
        .bind(q_id)
        .execute(&*database_pool)
        .await;

    if let Ok(question_deleted) = delete_result {
        // detect if rows were affected
        if question_deleted.rows_affected() > 0 {
            // if rows were affected, indicate question removed successfully
            let success_message = json!({"message":"Question deleted successfully"});
            (StatusCode::OK, Json(success_message)).into_response()
        } else {
            // if rows were NOT affected, indicate no question could be found
            let error_message =
                json!({"error":"Question with this specific if not found or it doesn't exists!"});
            (StatusCode::NOT_FOUND, Json(error_message)).into_response()
        }
    } else {
        // handle internal server error
        let error_message = json!({"error": "Internal server error"});
        (StatusCode::INTERNAL_SERVER_ERROR, Json(error_message)).into_response()
    }
}

// add questions to db from json object
pub async fn add_questions(
    State(database_pool): State<Arc<PgPool>>,
    Json(input): Json<Value>,
) -> impl IntoResponse {
    // verify the input is a correctly formatted series of questions
    if let Some(questions) = input.as_array() {
        // check each question and add to db if valid
        for question in questions {
            if let Err(error) = insert_question(&database_pool, question).await {
                return error.into_response();
            }
        }
        // indicate success if all questions added to db
        let success_message = json!({"message": "All questions added successfully"});
        (StatusCode::CREATED, Json(success_message)).into_response()
    } else if input.is_object() {
        // if input is a single object
        if let Err(error) = insert_question(&database_pool, &input).await {
            return error.into_response();
        }
        // indicate success if added to db
        let success_message = json!({"message": "Question added successfully"});
        (StatusCode::CREATED, Json(success_message)).into_response()
    } else {
        let error_message = json!({"error": "Invalid input format. Expected a single object or an array of objects."});
        (StatusCode::BAD_REQUEST, Json(error_message)).into_response()
    }
}

// adds a single question to db
async fn insert_question(
    database_pool: &Arc<PgPool>,
    question: &Value,
) -> Result<(), (StatusCode, Json<Value>)> {
    // parse question attributes
    let question_title = question
        .get("question_title")
        .and_then(|value| value.as_str());
    let type_of_content = question
        .get("type_of_content")
        .and_then(|value| value.as_str());
    let type_of_question = question
        .get("type_of_question")
        .and_then(|value| value.as_array());

    // validate all attributes of question
    if let (Some(question_title), Some(type_of_content), Some(type_of_question)) =
        (question_title, type_of_content, type_of_question)
    {
        // convert type_of_question to a String vector
        let type_of_question: Vec<String> = type_of_question
            .iter()
            .filter_map(|type_of_question| type_of_question.as_str().map(String::from))
            .collect();

        // add question to db
        let insert_result = sqlx::query(
            "INSERT INTO questions_table (question_title, type_of_content, type_of_question) VALUES ($1, $2, $3)",
        )
            .bind(question_title)
            .bind(type_of_content)
            .bind(&type_of_question)
            .execute(&**database_pool)
            .await;

        // handle internal server error
        if insert_result.is_err() {
            return Err((
                StatusCode::INTERNAL_SERVER_ERROR,
                Json(json!({"error": "Internal server error during insertion of question"})),
            ));
        }
        Ok(())
    } else {
        // indicate invalid/missing question attributes
        Err((
            StatusCode::BAD_REQUEST,
            Json(json!({"error": "Invalid input for a question. Must be missing inputs"})),
        ))
    }
}

// update question data in db
pub async fn update_question(
    Path(q_id): Path<i32>,
    State(database_pool): State<Arc<PgPool>>,
    Json(payload): Json<Value>,
) -> impl IntoResponse {
    // fetch the question from the db
    let select_query = sqlx::query_as::<_, QuestionStructure>(
        "SELECT * FROM questions_table WHERE question_id = $1",
    )
        .bind(q_id)
        .fetch_optional(&*database_pool)
        .await;
    // validate question successfully fetched
    if let Ok(question)= select_query {
        // validate that question exists
        if let Some(mut existing_question) = question {
            // update the question attributes with new data
            if let Some(question_title) = payload.get("question_title").and_then(|value| value.as_str()) {
                existing_question.question_title = question_title.to_string();
            }
            if let Some(type_of_content) = payload.get("type_of_content").and_then(|value| value.as_str()) {
                existing_question.type_of_content = type_of_content.to_string();
            }
            if let Some(type_of_question) =
                payload.get("type_of_question").and_then(|value| value.as_array())
            {
                existing_question.type_of_question = type_of_question
                    .iter()
                    .filter_map(|t| t.as_str().map(String::from))
                    .collect();
            }

            // update the question in the db
            let update_query = sqlx::query(
                "UPDATE questions_table SET question_title = $1, type_of_content = $2, type_of_question = $3 WHERE question_id = $4",
            )
                .bind(&existing_question.question_title)
                .bind(&existing_question.type_of_content)
                .bind(&existing_question.type_of_question)
                .bind(q_id)
                .execute(&*database_pool)
                .await;

            // validate that the update was successful
            if let Ok(_) = update_query {
                let success_message = json!({"message": "Question updated successfully"});
                (StatusCode::OK, Json(success_message)).into_response()
            } else {
                // indicate unsuccessful update
                let error_message = json!({"error": "Internal server error during update of question"});
                (StatusCode::INTERNAL_SERVER_ERROR, Json(error_message)).into_response()
            }
        } else {
            // indicate no question found
            let error_message = json!({"error":"Question with this specific ID not found or it doesn't exist!"});
            (StatusCode::NOT_FOUND, Json(error_message)).into_response()
        }
    } else {
        // indicate error fetching the question
        let error_message = json!({"error": "Internal server error"});
        (StatusCode::INTERNAL_SERVER_ERROR, Json(error_message)).into_response()
    }
}