mod questions_database;
mod request_handlers;
mod request_routes;

use crate::questions_database::initialize_questions_database;
use dotenv::dotenv;
use request_routes::setup_routes;
use std::sync::Arc;

#[tokio::main]
async fn main() {
    // open .env file to load the environment variables
    dotenv().ok();

    // establish pool of db connections
    let database_pool = Arc::new(initialize_questions_database().await);

    // establish routes for the server
    let routes = setup_routes(database_pool.clone());
    println!("Server up.");
    let tcp_listener = tokio::net::TcpListener::bind("127.0.0.1:3000").await.unwrap();
    axum::serve(tcp_listener, routes).await.unwrap();
}
