use sqlx::{PgPool, Pool, Postgres};
use std::env;

pub mod questions_module {
    use serde::{Deserialize, Serialize};
    use sqlx::FromRow;

    #[derive(Debug, Clone, Serialize, Deserialize, FromRow)]
    pub struct QuestionStructure {
        pub question_id: i32,
        pub question_title: String,
        pub type_of_content: String,
        pub type_of_question: Vec<String>,
    }
}

// create the postgres database connection pool from env variables
pub async fn initialize_questions_database() -> Pool<Postgres> {
    let question_database_url =
        env::var("QUESTION_DATABASE_URL").expect("QUESTION_DATABASE_URL must be set in .env file");
    PgPool::connect(&question_database_url)
        .await
        .expect("Couldn't connect to the database. Please check connection or database url")
}
