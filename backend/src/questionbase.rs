use crate::*;

/// An enumeration of errors that may occur
#[derive(Debug, thiserror::Error, ToSchema, Serialize)]
pub enum QuestionBaseErr {
    #[error("Question already exists: {0}")]
    QuestionExists(String),
    #[error("Questionbase io failed: {0}")]
    QuestionBaseIoError(String),
    #[error("Missing question payload")]
    NoQuestionPayload,
    #[error("Question {0} doesn't exist")]
    QuestionDoesNotExist(String),
    #[error("Question payload unprocessable")]
    QuestionUnprocessable(String),
    #[error("Invalid query parameter values")]
    QuestionPaginationInvalid(String),
}

impl From<std::io::Error> for QuestionBaseErr {
    /// Converts a `std::io::Error` into a `QuestionBaseErr`.
    ///
    /// # Description
    ///
    /// This allows `std::io::Error` instances to be converted into
    /// `QuestionBaseErr`, wrapping the I/O error as a `QuestionBaseIoError`.
    ///
    /// # Example
    ///
    /// ```
    /// let io_err = std::io::Error::new(std::io::ErrorKind::Other, "IO error");
    /// let question_base_err: QuestionBaseErr = io_err.into();
    /// ```
    fn from(e: std::io::Error) -> Self {
        QuestionBaseErr::QuestionBaseIoError(e.to_string())
    }
}

/// struct that represents a QuestionBase error, but include a `StatusCode`
/// in addition to a `QuestionBaseErr`
#[derive(Debug)]
pub struct QuestionBaseError {
    pub status: StatusCode,
    pub error: QuestionBaseErr,
}

/// Implements `ToSchema` trait for `QuestionBaseError` generating a JSON schema
/// for the error type
impl<'s> ToSchema<'s> for QuestionBaseError {
    /// Returns a JSON schema for `QuestionBaseError`
    ///
    /// The schema defines two properties:
    ///
    /// * `status`: A string representing the HTTP status code associated with the error.
    /// * `error`: A string describing the specific error that occurred.
    fn schema() -> (&'s str, RefOr<Schema>) {
        let sch = ObjectBuilder::new()
            .property(
                "status",
                ObjectBuilder::new().schema_type(SchemaType::String),
            )
            .property(
                "error",
                ObjectBuilder::new().schema_type(SchemaType::String),
            )
            .example(Some(serde_json::json!({
                "status":"404","error":"no question"
            })))
            .into();
        ("QuestionBaseError", sch)
    }
}

/// Implements the `Serialize` trait for `QuestionBaseError`
impl Serialize for QuestionBaseError {
    /// Serializes a `QuestionBaseError`
    ///
    /// The serialized JSON object will have two properties:
    ///
    /// * `status`: A string for the HTTP status code
    /// * `error`: A string describing the error
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let status: String = self.status.to_string();
        let mut state = serializer.serialize_struct("QuestionBaseError", 2)?;
        state.serialize_field("status", &status)?;
        state.serialize_field("error", &self.error)?;
        state.end()
    }
}

impl QuestionBaseError {
    /// Creates a `Response` instance from a `StatusCode` and `QuestionBaseErr`.
    ///
    /// # Parameters
    ///
    /// * `status`: The HTTP status code.
    /// * `error`: The `QuestionBaseErr` instance.
    ///
    /// # Returns
    ///
    /// `Response` instance with the status code and JSON body containing the error.
    pub fn response(status: StatusCode, error: QuestionBaseErr) -> Response {
        let error = QuestionBaseError { status, error };
        (status, Json(error)).into_response()
    }
}

/// A type alias for a `HashMap` of question IDs mapped to `Question` instances.
type QuestionMap = HashMap<String, Question>;

/// A question base that stores and manages questions
/// The question base is saved/loaded from a file on disk
#[derive(Debug)]
pub struct QuestionBase {
    file: File,
    questionmap: QuestionMap,
}

impl QuestionBase {
    /// Creates a new `QuestionBase` instance.
    ///
    /// # Parameters
    ///
    /// * `db_path`: The path to the file that will store the questions.
    ///
    /// # Returns
    ///
    /// A new `QuestionBase` instance, or an error if the file cannot be created or read.
    pub fn new<P: AsRef<std::path::Path>>(db_path: P) -> Result<Self, std::io::Error> {
        let mut file = File::create_new(&db_path)
            .and_then(|mut f| {
                let questionmap: QuestionMap = HashMap::new();
                let json = serde_json::to_string(&questionmap).unwrap();
                f.write_all(json.as_bytes())?;
                f.sync_all()?;
                f.rewind()?;
                Ok(f)
            })
            .or_else(|e| {
                if e.kind() == ErrorKind::AlreadyExists {
                    File::options().read(true).write(true).open(&db_path)
                } else {
                    Err(e)
                }
            })?;
        let json = std::io::read_to_string(&mut file)?;
        let questionmap = serde_json::from_str(&json)
            .map_err(|e| std::io::Error::new(ErrorKind::InvalidData, e))?;
        Ok(Self { file, questionmap })
    }

    /// Retrieves a paginated list of questions from the question base.
    ///
    /// # Parameters
    ///
    /// * `page`: The page number to retrieve (starts at 1)
    /// * `limit`: The number of questions to retrieve per page.
    ///
    /// # Returns
    ///
    /// A vector of tuples that are the question ID and reference to the question
    /// If the pagination parameters are invalid, returns a `QuestionBaseErr` error.
    pub fn paginated_get(
        &self,
        page: usize,
        limit: usize,
    ) -> Result<Vec<(String, &Question)>, QuestionBaseErr> {
        let total_questions = self.questionmap.len();
        let start_index = (page - 1) * limit;
        let end_index = start_index + limit;
        if start_index > total_questions {
            return Err(QuestionBaseErr::QuestionPaginationInvalid(
                "Invalid query parameter values".to_string(),
            ));
        }
        let question_vec: Vec<(_, _)> = self
            .questionmap
            .iter()
            .enumerate()
            .filter(|&(i, _)| i >= start_index && i < end_index)
            .map(|(_, (k, v))| (k.clone(), v))
            .collect();
        dbg!(&question_vec);
        Ok(question_vec)
    }

    /// Retrieves a random question.
    ///
    /// # Returns
    ///
    /// An `Option` containing a reference to a random `Question` or `None` if the question base is empty.
    pub fn get_random(&self) -> Option<&Question> {
        fastrand::choice(self.questionmap.iter()).map(|x| x.1)
    }

    /// Retrieves a question by its ID.
    ///
    /// # Parameters
    ///
    /// * `index`: The ID of the question.
    ///
    /// # Returns
    ///
    /// A reference to the `Question` instance with the specified ID, or a `QuestionBaseErr` error if the question does not exist.
    pub fn get<'a>(&'a self, index: &str) -> Result<&'a Question, QuestionBaseErr> {
        self.questionmap
            .get(index)
            .ok_or_else(|| QuestionBaseErr::QuestionDoesNotExist(index.to_string()))
    }

    /// Writes the current state of the question base to disk.
    ///
    /// This method serializes the `questionmap` to JSON and writes it to the file.
    ///
    /// # Returns
    ///
    /// A `Result` indicating whether the write operation was successful or had
    /// an IO error
    fn write_questions(&mut self) -> Result<(), std::io::Error> {
        let json = serde_json::to_string(&self.questionmap).unwrap();
        self.file.rewind()?;
        self.file.set_len(0)?;
        self.file.write_all(json.as_bytes())?;
        self.file.sync_all()
    }

    /// Adds a new question.
    ///
    /// # Parameters
    ///
    /// * `question`: The `Question` to add to the question base.
    ///
    /// # Returns
    ///
    /// A `Result` indicating whether the question was added successfully.
    /// If the question already exists, returns a `QuestionBaseErr` error.
    pub fn add(&mut self, question: Question) -> Result<(), QuestionBaseErr> {
        let id = question.id.clone();
        if self.questionmap.get(&id).is_some() {
            return Err(QuestionBaseErr::QuestionExists(id));
        }
        self.questionmap.insert(id, question);
        self.write_questions()?;
        Ok(())
    }

    /// Removes a question by its ID.
    ///
    /// # Parameters
    ///
    /// * `index`: The ID of the question.
    ///
    /// # Returns
    ///
    /// A `Result` indicating whether the question was removed successfully.
    /// If the question does not exist, returns a `QuestionBaseErr` error.
    pub fn delete(&mut self, index: &str) -> Result<(), QuestionBaseErr> {
        if !self.questionmap.contains_key(index) {
            return Err(QuestionBaseErr::QuestionDoesNotExist(index.to_string()));
        }
        self.questionmap.remove(index);
        self.write_questions()?;
        Ok(())
    }

    /// Updates a question by its ID.
    ///
    /// # Parameters
    ///
    /// * `index`: The ID of the question to update.
    /// * `question`: The updated `Question` instance.
    ///
    /// # Returns
    ///
    /// A `Result` indicating whether the question was updated successfully.
    /// If the question does not exist or is unprocessable, returns a `QuestionBaseErr` error.
    /// If successful, returns a `StatusCode` of 200.
    pub fn update(
        &mut self,
        index: &str,
        question: Question,
    ) -> Result<StatusCode, QuestionBaseErr> {
        if !self.questionmap.contains_key(index) {
            return Err(QuestionBaseErr::NoQuestionPayload);
        }
        if question.id.is_empty() {
            return Err(QuestionBaseErr::QuestionUnprocessable(index.to_string()));
        }
        self.questionmap
            .entry(index.to_string())
            .and_modify(|x| *x = question);
        self.write_questions()?;
        Ok(StatusCode::OK)
    }
}

/// Converts a `QuestionBase` reference into an HTTP response.
///
/// # Returns
///
/// A `Response` object with a status code of 200 OK and a JSON body containing the question map.
impl IntoResponse for &QuestionBase {
    fn into_response(self) -> Response {
        (StatusCode::OK, Json(&self.questionmap)).into_response()
    }
}