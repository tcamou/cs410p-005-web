# Rust Web Development
**CS 410P**
**Professor Bart Massey**

*IMPORTANT: my frontend is under the "main" branch of the repo, backend is under "master".*

Tommy Camou \
Spring 2024 \
This is a repo for Rust Web Development 

## Code derived from:
- Bart Massey 
- Rust Web Development: With Warp, Tokio, and Reqwest by Bastian Gruber (2023)
- Web tutorials and documentation for Axum
## License
With the content exceptions noted above, this work is licensed under the "MIT License". Please see the file LICENSE.txt in this distribution for license terms.